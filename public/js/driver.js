var selectRide = function(rideId) {
  $.ajax({
    method: "POST",
    url: "/driver/acceptRide",
    data: {
      rideId: rideId
    }
  })
}

$(document).ready(function() {
    $.ajax({
      method: "GET",
      url: "/driver/ridesData"
    }).done(function(data) {
      var now = Date.now();

      if(data['waitingRides'] && data['waitingRides'].length > 0) {
        data['waitingRides'].forEach(function(ride) {
          var temp = ride.split(':');
          var rideId = temp[1];
          var customer_id = temp[0];
          var timeDiff = now - parseInt(rideId);

          var timeDiffString = Math.floor(timeDiff/60000) + " Minutes " + Math.floor(timeDiff/1000)%60 + " Seconds";
          $('#waitingRides').append(
            '<li><div>' +
              'Req.Id: '+ ride +' </br>Cust.id:'+ customer_id +' </br> '+
              timeDiffString + ' ago</br>' +
              '<button onclick="selectRide(\''+ ride +'\')">Select</button>'+
            '</div></li>'
          )
        })
      }
      if(data['ongoingRides'] && data['ongoingRides'].length > 0) {
        data['ongoingRides'].forEach(function(ride) {
          ride = JSON.parse(ride);
          console.log(ride);
          var customer_id = ride.customer;
          var requestTime = ride.requestTime;
          var rideId = customer_id + ":" + requestTime;
          var pickupTime = ride.endTime - 5*60*1000;
          var timeDiffPickUp = Date.now() - pickupTime;
          var timeDiffRequest = Date.now() - requestTime;

          var timeDiffStringPickUp = Math.floor(timeDiffPickUp/60000) + " Minutes " + Math.floor(timeDiffPickUp/1000)%60 + " Seconds";
          var timeDiffStringRequest = Math.floor(timeDiffRequest/60000) + " Minutes " + Math.floor(timeDiffRequest/1000)%60 + " Seconds";

          $('#ongoingRides').append(
            '<li><div>' +
              'Req.Id: '+ rideId +' </br>Cust.id:'+ customer_id +' </br> '+
              'Request: '+ timeDiffStringRequest + 'ago</br>' +
              'Pickup: '+ timeDiffStringPickUp + ' ago' +
            '</div></li>'
          )
        })
      }
      if(data['completedRides'] && data['completedRides'].length > 0) {
        data['completedRides'].forEach(function(ride) {
          ride = JSON.parse(ride);

          var customer_id = ride.customer;
          var requestTime = ride.requestTime;
          var rideId = customer_id + ":" + requestTime;
          var pickupTime = ride.endTime - 5*60*1000;
          var timeDiffPickUp = Date.now() - pickupTime;
          var timeDiffRequest = Date.now() - requestTime;
          var timeDiffComplete = Date.now() - ride.endTime;

          var timeDiffStringPickUp = Math.floor(timeDiffPickUp/60000) + " Minutes " + Math.floor(timeDiffPickUp/1000)%60 + " Seconds";
          var timeDiffStringRequest = Math.floor(timeDiffRequest/60000) + " Minutes " + Math.floor(timeDiffRequest/1000)%60 + " Seconds";
          var timeDiffStringComplete = Math.floor(timeDiffComplete/60000) + " Minutes " + Math.floor(timeDiffComplete/1000)%60 + " Seconds";

          $('#completedRides').append(
            '<li><div>' +
              'Req.Id: '+ rideId +' </br>Cust.id:'+ customer_id +' </br> '+
              'Request: '+ timeDiffStringRequest + ' ago</br>' +
              'Pickup: '+ timeDiffStringPickUp + ' ago</br>' +
              'Complete: '+ timeDiffStringComplete + ' ago</br>' +
            '</div></li>'
          )
        })
      }
    }).error(function(error) {
      window.location = window.location.origin;
    })

})
