$(document).ready(function() {
  $.ajax({
    method: "GET",
    url: "/dashboard/allRides"
  }).done(function(data) {
    if(data['completedRides'] && data['completedRides'].length > 0) {
      data['completedRides'].forEach(function(ride) {
        var ride = JSON.parse(ride);
        var endTime = ride.endTime;
        var now = Date.now();
        var timeDiff = now - parseInt(endTime);
        var timeDiffString = Math.floor(timeDiff/60000) + " Minutes " + Math.floor(timeDiff/1000)%60 + " Seconds";

        $('#requestId').append(
          '<li>'+ride.customer+":"+ride.requestTime+'</li>'
        )
        $('#customerId').append(
          '<li>'+ride.customer+'</li>'
        )
        $('#timeElapsed').append(
          '<li>'+ timeDiffString +' ago</li>'
        )
        $('#status').append(
          '<li>Completed</li>'
        )
        $('#driver').append(
          '<li>'+ride.driver+'</li>'
        )
      })
    }
    if(data['ongoingRides'] && data['ongoingRides'].length > 0) {
      data['ongoingRides'].forEach(function(ride) {
        var ride = JSON.parse(ride);
        var startTime = ride.endTime - 5*60*1000;
        var now = Date.now();
        var timeDiff = now - parseInt(startTime);
        var timeDiffString = Math.floor(timeDiff/60000) + " Minutes " + Math.floor(timeDiff/1000)%60 + " Seconds";

        $('#requestId').append(
          '<li>'+ride.customer+":"+ride.requestTime+'</li>'
        )
        $('#customerId').append(
          '<li>'+ride.customer+'</li>'
        )
        $('#timeElapsed').append(
          '<li>'+ timeDiffString +' ago</li>'
        )
        $('#status').append(
          '<li>Ongoing</li>'
        )
        $('#driver').append(
          '<li>'+ride.driver+'</li>'
        )
      })
    }
    if(data['pendingRides'] && data['pendingRides'].length > 0) {
      data['pendingRides'].forEach(function(ride) {
        var ride = JSON.parse(ride);
        var requestTime = ride.requestTime;
        var now = Date.now();
        var timeDiff = now - parseInt(requestTime);
        var timeDiffString = Math.floor(timeDiff/60000) + " Minutes " + Math.floor(timeDiff/1000)%60 + " Seconds";

        $('#requestId').append(
          '<li>'+ride.customer+":"+ride.requestTime+'</li>'
        )
        $('#customerId').append(
          '<li>'+ride.customer+'</li>'
        )
        $('#timeElapsed').append(
          '<li>'+ timeDiffString +' ago</li>'
        )
        $('#status').append(
          '<li>Pending</li>'
        )
        $('#driver').append(
          '<li>'+ride.driver+'</li>'
        )
      })
    }
  })
})
