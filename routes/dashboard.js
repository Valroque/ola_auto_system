var router = require('express').Router();
var path = require('path');
var middlewares = require('./middlewares.js');
var utils = require('./utils.js');
var redisClient = utils.redisClient;
var checkAdminLogin = middlewares.checkAdminLogin;
var path = require('path');
var async = require('async');

router.get('/', checkAdminLogin, function(req, res) {
  res.sendFile(path.resolve(__dirname, '../views/dashboard/lpDashboard.html'));
})

router.post('/login', function(req, res) {
  var loginData = req.body;

  redisClient.sismember('admins', JSON.stringify(loginData), function(error, response) {
    if(error) {
      console.log(error);
      res.status(500);
      res.sendFile(path.resolve(__dirname, '../views/dashboard/dashboardLogin.html'));
    } else {
      if(response) {
        req.session.loggedIn = true;
        req.session.admin = true;
        req.session.admin_id = loginData.admin_id;
        req.session.password = loginData.password;
        res.status(200);
        res.sendFile(path.resolve(__dirname, '../views/dashboard/lpDashboard.html'));
      } else {
        res.status(401);
        res.sendFile(path.resolve(__dirname, '../views/dashboard/dashboardLogin.html'));
      }
    }
  })
})

router.get('/allRides', checkAdminLogin, function(req, res) {
  var now = Date.now();

  redisClient.lrange('ride_history', 0, -1, function(error, response) {
    var allRides = {
      'pendingRides' : [],
      'ongoingRides' : [],
      'completedRides' : []
    }

    if(error) {
      console.log(error);
      res.send(allRides);
      return;
    }

    async.eachSeries(response, function(ride, callback) {
      redisClient.get(ride, function(error, response) {
        if(error) {
          console.log(error);
          callback();
        } else {
          response = JSON.parse(response);
          if(response.endTime) {
            if(now > parseInt(response.endTime)) {
              allRides['completedRides'].push(JSON.stringify(response));
            } else {
              allRides['ongoingRides'].push(JSON.stringify(response));
            }
          } else {
            allRides['pendingRides'].push(JSON.stringify(response));
          }

          callback();
        }
      })
    }, function(error) {
      if(error) console.log(error)
      res.send(allRides);
    })
  })
})

module.exports = router;
