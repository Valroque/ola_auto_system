var router = require('express').Router();
var path = require('path');
var utils = require('./utils.js');
var redisClient = utils.redisClient;

router.get('/', function(req, res) {
  res.sendFile(path.resolve(__dirname, '../views/customer/lpCustomer.html'));
})

router.post('/newRide', function(req, res) {
  var timeStamp = new Date().valueOf();
  var customer = req.body['customer_id'].trim().toLowerCase();
  customer.replace(/ /g, "_");

  var key = customer + ':' + timeStamp.toString();
  var rideData = {
    requestTime : timeStamp.toString(),
    customer : customer
  }

  redisClient.sadd('pendingRides', key, function(error, response) {
    if(error) {
      console.log(error)
      res.status(500);
      res.send({'status':0, 'message':'Unable to book ride.'});
    } else {
      redisClient.lpush('ride_history', key, function(error, response) {
        if(error) {
          redisClient.srem('pendingRides', key);
          res.status(500);
          res.send({'status':0, 'message':'Unable to book ride.'});
          return;
        }
        redisClient.set(key, JSON.stringify(rideData), function(error) {
          if(error) {
            redisClient.srem('pendingRides', key);
            redisClient.rpop('ride_history', key);
            res.status(500);
            res.send({'status':0, 'message':'Unable to book ride.'});
            return;
          }
          res.status(200);
          res.redirect('/customer');
        })
      })
    }
  })
})

module.exports = router;
