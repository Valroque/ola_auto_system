var middlewares = require('./middlewares.js');
var checkLoggedIn = middlewares.checkLoggedIn;
var utils = require('./utils.js');
var redisClient = utils.redisClient;
var router = require('express').Router();
var path = require('path');
var async = require('async');

router.post('/login', function(req, res) {
  var driverData = req.body;

  redisClient.sismember('drivers', "{\"driver_id\":\"Driver1\",\"password\":\"Drvier1\"}", function(error, response) {
    if(error) {
      console.log(error);
      res.sendFile(path.resolve(__dirname, '../views/driver/driverLogin.html'));
    } else {
      if(response == 1) {
        req.session.loggedIn = true;
        req.session.driver_id = driverData.driver_id;
        req.session.password = driverData.password;
        console.log(req.session);
        res.sendFile(path.resolve(__dirname, '../views/driver/lpDriver.html'));
      } else {
        req.session.loggedIn = false;
        res.sendFile(path.resolve(__dirname, '../views/driver/driverLogin.html'));
      }
    }
  })

})

router.get('/', checkLoggedIn, function(req, res) {
  res.sendFile(path.resolve(__dirname, '../views/driver/lpDriver.html'));
})

var getWaitingRides = function(mcallback) {

}

router.get('/ridesData', checkLoggedIn, function(req, res) {
  req.session.ometing = "asdasd";
  async.parallel([
    function(mcallback) {
      redisClient.smembers('pendingRides', function(error, response) {
        if(error) {
          console.log(error);
          mcallback(error, []);
        } else {
          mcallback(null, response);
        }
      })
    },
    function(mcallback) {
      var key = req.session.driver_id + ':rides';
      var ridesData = {
        completedRides : [],
        ongoingRides : []
      }
      redisClient.lrange(key, 0, -1, function(error, response) {
        async.eachSeries(response, function(rideId, callback) {
          redisClient.get(rideId, function(error, response) {
            var rideData = JSON.parse(response);
            if(Date.now() > rideData.endTime) {
              ridesData['completedRides'].push(JSON.stringify(rideData));
            } else {
              ridesData['ongoingRides'].push(JSON.stringify(rideData));
            }
            callback();
          })
        }, function(error) {
          if(error) {
            mcallback(error, null);
          } else {
            mcallback(null, ridesData);
          }
        })
      })
    }
  ], function(error, result) {
    if(error) console.log(error);
    res.send({
      'waitingRides' : result[0],
      'ongoingRides' : result[1]['ongoingRides'],
      'completedRides' : result[1]['completedRides']
    });
  })
})

router.post('/acceptRide', checkLoggedIn, function(req, res) {
  var rideId = req.body.rideId;
  var driverId = req.session.driver_id;
  var rideEndTime = (Date.now() + 5*60*1000).toString();
  var temp = rideId.split(':');
  var customerId = temp[0];
  var rideRequestTime = temp[1];
  var rideData = {
    requestTime : rideRequestTime,
    endTime : rideEndTime,
    customer : customerId,
    driver : driverId
  }
  console.log(rideData, req.session);
  rideData = JSON.stringify(rideData);

  redisClient.srem('pendingRides', req.body.rideId, function(error, response) {
    if(error) {
      console.log(error);
      res.send({'status':0, 'message':'Unable to accept ride'});
    } else {
      if(response) {
        try {
          redisClient.lpush(driverId + ':rides', rideId, function(error) {
            if(error) throw error;
          })
          redisClient.set(rideId, rideData, function(error) {
            if(error) throw error;
          })
          redisClient.set('live:' + rideId, "empty", function(error) {
            if(error) throw error;
            redisClient.expire('live:' + rideId, 300, function(error) {
              if(error) throw error;
            })
          })
        } catch (e) {
          console.log(e);
        } finally {
          res.status(200);
          res.send({'status':1, 'message':'Ride Accepted'});
        }
      }
    }
  })
})

module.exports = router;
