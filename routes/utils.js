var redis = require("redis");
var redisServer = {
  'host' : 'localhost',
  'port' : '6379'
}

var redisClient = redis.createClient(redisServer);
exports.redisClient = redisClient;
