var path = require('path');

//middlewares
var checkLoggedIn = function(req, res, next) {
  if(req.body && req.session.loggedIn) {
    next();
  } else {
    res.sendFile(path.resolve(__dirname, '../views/driver/driverLogin.html'));
  }
}
exports.checkLoggedIn = checkLoggedIn;

var checkAdminLogin = function(req, res, next) {
  if(req.session.loggedIn && req.session.admin) {
    next();
  } else {
    res.sendFile(path.resolve(__dirname, '../views/dashboard/dashboardLogin.html'));
  }
}
exports.checkAdminLogin = checkAdminLogin;
