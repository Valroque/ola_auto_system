const express = require('express');
const app = express();
var bodyParser = require('body-parser');
var http = require('http').Server(app);
var port = process.env.PORT || 3000;
var utils = require('./routes/utils.js');
var redisClient = utils.redisClient;
var session = require('express-session');
var redisStore = require('connect-redis')(session);
//var io = require('socket.io')(http);

var appSession = session({
  secret: 'keyboard cat',
  resave: true,
  saveUninitialized: true,
  cookie: { secure: false, path: '/', maxAge: 10000, httpOnly: false},
  rolling: true,
  store: new redisStore({
   host: 'localhost',
    port: 6379,
    disableTTL: true
  })
});

app.use(express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/', function(req, res) {
  res.sendFile(__dirname + '/views/index.html');
});

//customer
app.use('/customer', appSession, require('./routes/customer.js'));

//dashboard
app.use('/dashboard', appSession, require('./routes/dashboard.js'));

//driver
app.use('/driver', appSession, require('./routes/driver.js'));

app.get('/logout', appSession, function(req, res) {
  req.session.destroy();
  res.redirect('/');
})

http.listen(port, function(){
  console.log('listening on *:', port);
});
