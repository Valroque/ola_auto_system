var redis = require("redis");
var async = require("async");
var redisServer = {
  'host' : 'localhost',
  'port' : '6379'
}

var redisClient = redis.createClient(redisServer);

var createDrivers = function(mcallback) {
  var driver1 = {
    driver_id : 'Driver1',
    password : 'Drvier1'
  }
  var driver2 = {
    driver_id : 'Driver2',
    password : 'Driver2'
  }
  var driver3 = {
    driver_id : 'Driver3',
    password : 'Driver3'
  }
  var driver4 = {
    driver_id : 'Driver4',
    password : 'Driver4'
  }
  var driver5 = {
    driver_id : 'Driver5',
    password : 'Driver5'
  }

  var driverData = [JSON.stringify(driver1), JSON.stringify(driver2), JSON.stringify(driver3), JSON.stringify(driver4), JSON.stringify(driver5)];

  async.eachSeries(driverData, function(driver, callback){
    redisClient.sadd('drivers', driver, function(error) {
      if(error) {
        console.log(error);
      }
      callback();
    })
  }, function(error){
    if(error)console.log(error);
    mcallback();
  })
}

var createAdmins = function(callback) {
  var admin = {
    admin_id : 'Admin',
    password : 'Admin'
  }

  redisClient.sadd('admins', JSON.stringify(admin), function(error) {
    if(error) {
      console.log(error);
    }
    callback();
  })
}

createDrivers(function() {
  console.log("All driver data set!");
  createAdmins(function() {
    console.log("All admins data set!");
    process.exit();
  })
});
